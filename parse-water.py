# Avani Wildani
# This code is to parse the data from the water management facility.
# Headers:
# "WebSite Visit ID"  "Date"  "IP Address"  "Site"  "Site Type" "District"
# "Year"  "File"
#
#

import sys
import datetime
import random
#import matplotlib
#import pylab

def list_avg(mylist):
  return(sum(mylist)/float(len(mylist)))

def replacesep(file_obj,oldsep,newsep,outfilename):
  outfile = open(outfilename,'w')
  for line in file_obj:
    l = line.replace(oldsep,newsep)
    outfile.write(l)

def sec(datetime_obj):
  return datetime_obj.days*(60*60*24) + datetime_obj.seconds

def always_on(runtime,files_per_disk,cost_on,read_cost,var,numfiles = 25971,numreads=92250):
  num_disks = numfiles/float(files_per_disk)
  rsec = sec(runtime)
  always_on_power = (rsec*cost_on*num_disks) + (numreads * read_cost)
  # We assume that the disk is idling most of the time, even though it is still
  # 'on'.  The extra read cost is from the seek for the read.
  return locals()[var], always_on_power/1000
  #print "Always on "+str(always_on_power/1000)+"kJ."


def get_random(spintime,runtime,num):
  """Want to return a list of randomly selected points for spinup during the selected runtime"""
  rsec = sec(runtime)
  spins = set()
  for i in range(num):
    spins.add(int(random.random()*rsec))
  spinslist = list(spins)
  spinslist.sort()    # Sorts spin events
  spinslist.reverse() # So we can pop off more recent events later on.
  return spinslist

def random_hit(time,random_list,spintime):
  if len(random_list) == 0:
    return False
  start = datetime.datetime(2007, 10, 1, 13, 49, 15)
  mytime = time - start
  mytime_sec = sec(mytime)
  next_time = random_list.pop() 
  while next_time <= sec(mytime-spintime): 
    next_time = random_list.pop() # Knock out random windows we've missed
  if next_time >= sec(mytime-spintime) and next_time <= sec(mytime+spintime):
    return True
  else:
    return False

def sitevar(file_obj,group_type,types,google,random_ons,spintime = 49,fgsize = 1,ts=5,ontime=.5):
  # This function returns the percentage of data that is within the same group
  #  and accessed within disk-spin-time. 
  # Current results (4/27/2010): 
  # python parse-water.py ../data/water-data.csv 
  #   We had a 0.521517615176 hit rate at 100s.
  # python parse-water.py ../data/water-data.csv 
  #   We had a 0.424531165312 hit rate at 50s.
  starttime = "10/1/2007 13:49:15"
  endtime = "12/2/2009 7:32:54"
  start = datetime.datetime.strptime(starttime,"%m/%d/%Y %H:%M:%S")
  end = datetime.datetime.strptime(endtime,"%m/%d/%Y %H:%M:%S")
  random_list = []
  if random_ons > 0:
    random_list = get_random(spintime,(end-start),random_ons)
  disk_spin_time = datetime.timedelta(seconds=spintime)
  googleIPs = ['"66.249.67.14"','"66.249.67.11"','"66.249.67.17"','"66.249.65.225"','"63.145.252.35"']
  datehist = ''
  # labelhist will be of the form {SITE1:timeaccessed,SITE2:timeaccessed,...}.
  # Now we have to calculate timedeltas for everything :(
  labelhist = {}
  hit_count = 0
  s_count = 0 # A counter for consecutive reads
  singleton = 0
  line_count = 0
  randomhit_count = 0
  power= 0       # Let's do a better estimate of total power: my numbers felt
                 # high.
  spinup = 100   
  read = 20
  
  files = {} # We need the number of unique files for counting our leave-all-on
             # base case.
  groups = {} # Tracks the size of the different subgroups.
  readsize = [] # Tracks the number of files involved in consecutive reads.
  trash = file_obj.readline()

  timestep = datetime.timedelta(seconds=ts)
  timemark = start
  output = open('time_series_'+types[group_type]+'_'+str(ontime)+"_"+str(spintime)+'_'+str(fgsize),'w')
  for line in file_obj:
    l = line.strip().split(',')
    date,timestring = l[1].split()
    if google == 0:
      IP = l[2]
      if IP in googleIPs:
        continue
    time = datetime.datetime.strptime(l[1],"%m/%d/%Y %H:%M:%S")
    timediff = abs(time - timemark) 
    if timediff >= timestep:
      diff = time-start
      apower = always_on(diff,100,ontime,read,'rsec')[1] 
      output.write(str(diff.days*(60*60*24)+diff.seconds)+' '+str(power)+' '+str(apower)+'\n')
      timemark = time
    myfile = l[3]+l[6]+l[7]
    if myfile in files:
      files[myfile] += 1
    else:
      files[myfile] = 1
    #if time_last == '':
    #  time_last = time
    district = l[group_type]

    # File Group Size
    if district in groups:
      groups[district]+=1
    else:
      groups[district]=1
    # Let's find out if something is spinning  
    if random_hit(time,random_list,disk_spin_time):
      randomhit_count += 1
      power += read + ontime * spintime * fgsize
    elif date == datehist:
#      timedelta = abs(time - time_last)
#      if district == districthist and timedelta.seconds < disk_spin_time:
#        hit_count += 1
#        s_count += 1
       if district in labelhist:
         time_old = labelhist[district]
         timedelta = abs(time - time_old)
         if timedelta < disk_spin_time:
           hit_count += 1
           s_count += 1
           # Add read + 50s spin
           power += read + ontime * spintime * fgsize
           # Subtract what was left of the old spinup
           power -= timedelta.seconds * ontime * fgsize
         else: 
           if s_count == 0:
             singleton += 1
           else:
             readsize.append(s_count+1)
           s_count = 0
           power += spinup + read + spintime * ontime * fgsize
        #districthist = district
    else:
      power += spinup + read + spintime * ontime * fgsize
      datehist = date
    labelhist[district] = time
    #time_last = time
    line_count += 1

  starttime = "10/1/2007 13:49:15"
  endtime = "12/2/2009 7:32:54"
  
#  print "We have "+str(len(files))+" unique files."
  print "We have "+str(randomhit_count)+" random hits of "+str(random_ons)+' random hits.'
  print "We had a "+str(100*hit_count/float(line_count))+"% hit rate for "+types[group_type]+" at "+str(disk_spin_time)+"s ("+str(hit_count)+" of "+str(line_count)+")." 
#  print "There are "+str(100*(singleton/float(line_count)))+"% singletons ("+str(singleton)+" of "+str(line_count)+")."  
# For each hit,I save 100J spinup.  However, for each singleton I add an
# unneccessary 50J.
  #power_savings = (hit_count * spinup) - singleton*(ontime*spintime)
  #power_base = float(line_count * 100 + line_count*5)
#  power_base = line_count * (spinup + read)
#  power_percent = float(power_base - power) / power_base * 100
#  power_nospin_percent = float(always_on_power - power) / always_on_power * 100
#  print "We used "+str(power/1000)+"kJ."
#  print "We saved "+str(power_savings/1000)+"kJ, or "+str(power_percent)+'% of our expected '+str(power_base/1000)+'kJ.'
#  print "We saved "+str((power_base - power)/1000)+"kJ, or "+str(power_percent)+'% of our expected '+str(power_base/1000)+'kJ for every read costing a spin-up.'
#  print "We saved "+str((power_base - power)/1000)+"kJ, or "+str(power_percent)+'% of our expected '+str(power_base/1000)+'kJ for every read costing a spin-up.'
  #print groups
#  avg = list_avg([a[1] for a in groups.items()])
#  print "Average group size is "+str(avg)+" files."

def insert_failure(start_time,delta,groups,failure_log='failure_log'):
  # Inserts a failure at a time 't' within timedelta 'range' and a group 'g'
  # within 'groups'.  
  log = open(failure_log,'w')
  days_to_failure = datetime.timedelta(days = delta.days * random.random())
  fail_date = start_time + abs(days_to_failure)
  fail_group = random.choice(groups)
  line = 'Failed at '+fail_date.ctime()+' in group '+fail_group+'.\n'
  log.write(line)
  print line
  return fail_date,fail_group

def fail_sim(file_obj,types,startdate,timedelta,num_failures = 1):
  '''
  This function will insert a failure into the chosen group type and 
  '''
  failures = []
  for f in range(num_failures):
    failure.append(insert_failure(startdate,timedelta,[types[a] for a in types.keys()]))
  # Go through lines until time is < the least of the failures.
  # Make sure your rebuild rates are proportional to the size of your data.
  return True

if __name__ == "__main__":
  exp = ""
  cost_sec_on = 1
  read_cost = 4
  
  # This is specific to the current water data file (5/2)  Fix for other files.
  types = {5:'District',3:'Site',4:'Site Type',6:'Year'}
  starttime = "10/1/2007 13:49:15"
  endtime = "12/2/2009 7:32:54"
  start = datetime.datetime.strptime(starttime,"%m/%d/%Y %H:%M:%S")
  end = datetime.datetime.strptime(endtime,"%m/%d/%Y %H:%M:%S")
  runtime = end-start

  for arg,index in zip(sys.argv,range(len(sys.argv))):
    if arg == '-type':
      type = int(sys.argv[index+1])
    elif arg == '-file':
      # Report on file statistics
#      for type in [3,4,6]:
#        for st in [5,10,20,30,40,45,50,55,60,70,80,90,100,500,1000]:
#          for ontime in [.1,.3,.6,1,1.3,1.6,2,2.3,2.6,3,5,7]:
      file = sys.argv[index+1]
      file_obj = open(file,'r')
      sitevar(file_obj,5,types,1,90000,49,1,0,1)
    elif arg == '-spincost':
      spinup = int(sys.argv[index+1])
    elif arg == '-timeon':
      timeon = int(sys.argv[index+1])
    elif arg == '-power':
      power = sys.argv[index+1]
    elif arg == '-exp':
      exp = sys.argv[index+1]
  if exp == "files-disk":
    output = open('always_on_files_exp.csv','w')
    output.write('# Vary the number of disks.  1J/sec on, 4J/read\n')
    for f in [16,32,64,128,256,512,1024,2048,4096,8192]:
      var,ao_power = always_on(runtime,f,cost_sec_on,read_cost,'num_disks')
      output.write(str(var)+' '+str(ao_power)+'\n')
  #elif exp == "vsspinup":
    # I want to make a time series graph that shows power increasing in both
    # always-on and my clusters over time.
    

  # Change the format of the file.
  #replacesep(file_obj,'\t',',',"water-data.csv")

  # Simulate a localized failure scenario.
  #fail_sim(file_obj,types,startdate,timedelta)
