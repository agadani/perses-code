import sys
import math
'''Takes the results of of multiple runs averages them, producing error bars'''
# Currently set up so tha the character field is the unique name.  MODIFY THIS
# BEFORE USING IT ON OTHER PROJECTS.

def mean(l):
  return sum(l)/float(len(l))

def std_dev(l,mean):
  return math.sqrt(sum([(x-mean)**2 for x in l])/float(len(l)))

def average_and_error_bar(name_to_data_dict,conf_level=1):
  namelist = []
  for name in name_to_data_dict:
    outlist = [name]
    toplist = name_to_data_dict[name]
    for i in range(len(toplist[0])):
      l = [x[i] for x in toplist]
      l_mean = mean(l)
      l_stddev = std_dev(l,l_mean)
      outlist.append(l_mean)
      outlist.append(l_stddev*conf_level)
    namelist.append(outlist)
  return namelist

confidence = 1
run_file = open(sys.argv[1],'r')
run_name_dict = {}
for line in run_file:
  if line.startswith('#'): 
    continue
  l = line.strip().split(',')
  name = ','.join(map(str,l[0:2]))
  if name in run_name_dict:
    run_name_dict[name].append(map(float,l[2:]))
  else:
    run_name_dict[name]=[map(float,l[2:])]

outlist=average_and_error_bar(run_name_dict,confidence)
print "# Confidence interval is",confidence
for a in outlist:
  print ','.join(map(str,a))
    

  

