import csv
import sys
import datetime
import random

def parse_water(fileobj):
  # Parses csv water data and returns an array
  myarray=[]
  myreader = csv.reader(fileobj,delimiter=',',quotechar='"')
  for row in myreader:
    myarray.append(row)
  return myarray

def count_unique_elements(myarray):
  # Returns the number of unique files accessed.
  file_dict = {} # Do this as a dict so we can return max file counts later if
                 # we want to.
  for access in myarray:
    file = access[3]+access[-1]
    if file in file_dict:
      file_dict[file] += 1
    else:
      file_dict[file] = 1
  return len(file_dict.keys()) 

def count_group_elements(myarray, groupindex):
  # Returns the number of unique members of a group type
  # This not return the number of unique elements in a group: a different
  # function should do that.
  groups = {}
  for access in myarray:
    if access[groupindex].strip() in groups:
      groups[access[groupindex].strip()] += 1
    else:
      groups[access[groupindex].strip()] = 1
  return groups

def print_hist(groups):
  for group in groups:
    print group,groups[group] 


if __name__=="__main__":
  types = {5:'District',3:'Site',4:'Site Type',6:'Year'}
  fileobj = open(sys.argv[1],'r')
  fileobj.readline() # Knock out labels
  myarray = parse_water(fileobj)
  arg = int(sys.argv[2])
  if arg <= 6:
    groups = count_group_elements(myarray,arg)
    print "The number of elements in group "+str(types[arg])+" is: "+str(len(groups))
    print_hist(groups)
  else:
    print "The number of unique elements is: "+str(count_unique_elements(myarray))
