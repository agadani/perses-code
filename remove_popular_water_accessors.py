import water_lib
import sys

def most_popular_IPs(accesses,num_IPs):
  IP_dict={}
  for line in accesses:
    IP = line[2]
    if IP in IP_dict:
      IP_dict[IP] += 1
    else:
      IP_dict[IP]=1
  tots = [[IP_dict[x],x] for x in IP_dict.keys()]
  tots.sort()
  tots.reverse()
  return tots[:num_IPs]

def remove_IPs(accesses,IPs,outputfile):
  for access in accesses:
    if access[2] in IPs:
      continue
    outputfile.write(','.join(access)+'\n')

if __name__ == '__main__':
  fo = open(sys.argv[1],'r')
  accesses = water_lib.parse_water(fo)
  num_IPs = int(sys.argv[2])
  top_IPs= most_popular_IPs(accesses,num_IPs)
  total_cut = sum([x[0] for x in top_IPs])
  top_IPs = [x[1] for x in top_IPs]
  if sys.argv[3] == 'show':
    print 'The top IPs '+ str(top_IPs)+' account for '+str(total_cut)+' accesses out of '+str(len(accesses))
    print total_cut
  elif sys.argv[3] == 'remove':
    output = open(fo.name+'_sans_top_'+str(num_IPs)+'_IPs','w')
    remove_IPs(accesses,top_IPs,output)
    print 'Output written to '+output.name
