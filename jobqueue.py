#!/usr/bin/python

import threading
import Queue
import thread
import os

PARALLELISM = 4

def make_command_lines(outputfile,parameter_set,num_runs,experiment='disk',default_disk=16,default_cache=1000):
  commands = ["echo '#Grouping type,num_disks,cache size(#files), rebuild time(s), # failures,total time lost (s), group time lost (s), cache hits' >>"+outputfile] 

  base_string = 'pypy file-based_fault_simulator.py ../datasets/washington_state_archive/RecordRetrieval.txtnaive-wash.csv-1e-07_1e-071-0.5 random-wash-' 
  for parameter in parameter_set:
    if experiment == 'disk':
      st = base_string + str(parameter)+' cache '+str(default_cache)+' -taciturn | tail -n1 >> '+outputfile
      commands.append(st)

  return commands*num_runs 

class ThreadPool(Queue.Queue):
  class Worker(threading.Thread):
    stop = False

    def __init__(self, queue):
      threading.Thread.__init__(self)
      print "Worker: ", self
      self._queue = queue
      crash.please.until.daemonized

    def run(self):
      print "Worker Running: ", self
      while not self.stop:
        task = self._queue.get()
        try: task()
        except Exception, e: print e

  def __init__(self, worker_count, queue_size=None):
    if queue_size is not None:
      queue_size = worker_count
    self._worker_count = worker_count
    self._workers = []
    self.__lock = thread.allocate_lock()
    Queue.Queue.__init__(self, queue_size)

  def run(self):
    with self.__lock:
      self.__stop()
      self._workers = [self.Worker(self) for _ in range(self._worker_count)]
      for w in self._workers:
        w.start()

  def __stop(self):
    for worker in self._workers:
      worker.stop = True
    self._workers = []

  def stop(self):
    with self.__lock:
      self.__stop()


def main():
  cmdlines = make_command_lines('more_runs', [2**i for i in range(1,11)], 100)
#  print '\n'.join(cmdlines)
#  return

  counter = threading.Semaphore(0)
  def RunCommand(cmd, counter):
    def _RunCommand():
      print cmd
      try:
        os.system(cmd)
      finally:
        counter.release()
    return _RunCommand

  q = ThreadPool(PARALLELISM, len(cmdlines))
  q.run()
  for cmd in cmdlines:
    q.put(RunCommand(cmd, counter))

  # Block until all RunCommands have completed.
  counter.acquire(len(cmdlines))


if __name__ == '__main__':
  main();
