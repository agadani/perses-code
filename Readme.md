This is code for the paper " PERSES: Data Layout for Low Impact Failures",
published in MASCOTS 2014.  It is designed to take an I/O trace as input and lay
out the data on disks according to a predictive grouping.  It then can replay the trace, injecting failures and rebuilding
data as needed.

This code is "as-is" and unsupported.  
