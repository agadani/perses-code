import sys
import water_lib
import wash_lib
import threading
import random
import math
import datetime
import argparse

'''Assigns elements to disks and then runs twp threads: one for accessing data
(running through the trace: Thread TRACE) and another for inserting failures and
performing rebuids: Thread FAILER.  The only interactions between the threads is
that an access to a Disk by TRACE increases the probability that disk will fail
in FAILER and TRACE needs to check FAILER before every access to see if that
disk is failed or rebuilding.  If it is failed or rebuilding, it adds the
rebuild time to its 'lost time' total'''

# "WebSite Visit ID","Date","IP Address","Site","Site
# Type","District","Year","File"
#1250,10/1/2007 13:49:15,"72.166.5.82","16N2W5B1M_EXT","GW","ND","POR","GROUND_SURFACE_DISPLACEMENT_MEAN_PLOT.PNG"


# it may be easier to skip threads entirely and have each disk maintain a global
# "when will I fail" for the (known) length of the trace.  This also makes
# failing easy.

#global DISKNUM = 0

class Disk:
  '''A disk is a bucket to hold information about the files on disk and failure
  likelihood'''
  def __init__(self, files ,DISKNUM = '',failure_probability=.1):
    self.files = files
    index = {}
    for file in files:
      index[file.name] = file
    self.file_index = index
    self.failure_probability = failure_probability
    self.failure_count = 0
    self.spinups = 0
    self.disknum = DISKNUM
#    DISKNUM += 1
    self.time_lost = datetime.timedelta(seconds=0)
    self.last_failed = datetime.datetime(2000,10,1,13,49,15) # Picked a random
    # time after the trace we had ends 
    self.last_rebuild = datetime.datetime(2000,10,1,13,49,15)
    self.last_spun = datetime.datetime(2000,10,1,13,49,15) # Picked a random
    # time before the trace we had ends 
    self.last_scrubbed = datetime.datetime(2000,10,1,13,49,15) # Picked a random
    # time before the trace we had ends 
 
  def spin(self,time):
    self.spinups += 1
    self.last_spun = time

  def scrub(self,time,scrub_factor = .01):
    # Time is time in the space of the trace
    self.spin(time)
    self.failure_probability -= scrub_factor
    self.last_scrubbed = time


# Want to add the ability for 
class Stripe:
  '''Reliability Group'''
  def __init__(self,disks,rebuild_time,parity=1):
    self.disks = disks
    self.parity = parity
    self.parity_disks = []
    self.groups = set([])
    self.last_rebuild = datetime.datetime(2000,10,1,13,49,15) # after trace
    for i in range(parity):
      self.parity_disks.append(Disk(['parity']))
      
#  def get_groups(self):
#    for disk in self.disks:
#
#  def catas(self,time):
#    for disk in self.disks:
#

  def rebuild(self,time,downdisk):
    if time-self.last_rebuild < rebuild_time: 
      # Uh-oh: Full system failure!! Must go to backup
      self.catas(time)
    else:
      for disk in self.disks:
        disk.spin(time)
        disk.last_rebuild = time


class Filegroup:
  '''We need to keep track of file groups to enforce the belief that group
  members can not act if one member is unavailable'''
  def __init__(self,members,name="",disks=[]):
    self.members = members
    self.name = name.strip()
    self.disks = disks
    self.last_failure = datetime.datetime(2000,10,1,13,49,15) # Picked a random
    # time after the trace we had ends 
    self.time_lost = datetime.timedelta(seconds=0)
  def __repr__(self):
    return str(len(self.members))

class Access:
  '''An access is a read request from a user to the water website or a scrubbing
  operation'''
  def __init__(self,time,file,type,filegroup=""):
    self.file = file  # file accessed ("" if scrub)
    self.time = time  # access time
    self.type = type  # type = Read or Scrub
    self.filegroup = filegroup

class Cache: 
  '''
    LRU Cache that takes out hot elements. 
    * Cache is in units of "Number of files"
    * ALL FILES ARE THE SAME SIZE 
  '''
  def __init__(self,cache_size,members=[]):
    self.cache_size = cache_size
    self.members = members
    self.hits = 0

  def has(self,incoming_file):
    return incoming_file in self.members

  def markhit(self,incoming_file):
    self.hits += 1
    self.members.remove(incoming_file)
    self.members.append(incoming_file) # We assume this happens atomically, so
                                       # self.add is unnecessary
  def add(self,new_file):
    if len(self.members) < self.cache_size:
      self.members.append(new_file)
    elif cache_size < 0:
      self.members.pop(0)
      self.members.append(new_file)


class File:
  def __init__(self,name,site,site_type,district,year,wash=""):
    self.name = name
    self.site = site
    self.site_type = site_type
    self.district = district
    self.year = year
    self.wash = wash
    self.group = ""
    self.disk = ""

def get_disk(file,disks):
  '''Returns the disk a particular file is on'''
  for disk in disks:
    for f in disk.files:
      if f.name == file:
        return disk
  print "Couldn't find file "+str(file)
  sys.exit()


def get_group(file,groups):
  '''Returns the group a particular file is on'''
  for group in groups:
    for f in group.members:
      if f.name == file:
        return group
  print "Couldn't find file "+str(file)
  sys.exit()

#def disk_scrubber(disks,time,uptime=datetime.timedelta(100),scrubtime=datetime.timedelta(50)):
#  '''This function will be run by a thread in the background.  It will look
#  through disks checking time since last fail and last scrub.  If the time since
#  last fail is greater than uptime and the time since last scrub is greater than
#  scrubtime, the scrub function on the disk is called'''
#  while True:
#    for disk in disks:
#      now = date
#      if disk.last_failed > uptime and disk.last_scrubbed > scrubtime:
#        disk.scrub()

def threadless_disk_scrubber(disk,current_time,uptime,scrubtime):
  for disk in disks:
    if disk.last_failed-current_time < uptime and disk.last_scrubbed-current_time < scrubtime:
      disk.scrub()


def make_datetime(timestring,wash=False):
  # returns a datetime object from a time string
  if wash:
    return datetime.datetime.strptime(timestring,"%Y/%m/%d %H:%M:%S")
  else:
    return datetime.datetime.strptime(timestring,"%m/%d/%Y %H:%M:%S")

def turn_water_access_file_into_accesses(fo):
  '''Returns the list of accesses along with unique files'''
  lines = water_lib.parse_water(fo)
  accessess = []
  files = {} # Dictionary mapping filename:file_object
  for line in lines:
    time = make_datetime(line[1])
    site = line[3]
    file = line[3]+line[-1]
    if file not in files:
      site_type = line[4]
      district = line[5]
      year = line[6]
      files[file]=File(file,site,site_type,district,year)
    type = 'Read'
    accessess.append(Access(time,files[file],type))
  # add in scrubbing later
  filenames=files.keys()
  file_obs = [files[a] for a in filenames]
  return accessess, filenames, file_obs  


def turn_wash_access_file_into_accesses(fo):
  '''Returns the list of accesses along with unique files'''
  lines = wash_lib.parse_wash(fo)
  accessess = []
  files = {}
  no_id = 0
  max_id = 82982451
  for line in lines:
    grouping = line[0]
    time = make_datetime(line[4].split('.')[0].replace('-','/'),True)
    file = line[3]
    if file not in files:
      if file == '':
        no_id += 1
        file = max_id + no_id
      files[file] = File(file,"","","","",grouping)
    type = 'Read'
    accessess.append(Access(time,files[file],type))
  # add in scrubbing later
  filenames=files.keys()
  file_obs = [files[a] for a in filenames]
  print 'There were',no_id,'records with no ID that were added at the end'
  return accessess, filenames, file_obs  

def assign_files_in_temp_order(filenames,numdisks,group):
  disks = []
  group_dict = {}
  f_list = []
  len_files = len(filenames)
  for f in filenames:
    file_group = f.__dict__[group]
    if file_group not in group_dict:
      group_dict[file_group] = set([f])
    else:
      group_dict[file_group].add(f)
  
  for fg in group_dict:
    filegroup = Filegroup(group_dict[fg],fg)
    for f in group_dict[fg]:
      f.group = filegroup
    groups.append(filegroup)
    
  max_files_per_disk = int(math.ceil(len_files/float(numdisks)))
  fileindex=0
  for i in range(numdisks):
    if fileindex+max_files_per_disk < len_files:
      files = filenames[fileindex:fileindex+max_files_per_disk]
    else:
      files = filenames[fileindex:]
    fileindex = fileindex+max_files_per_disk
    j = Disk(files)
    for fo in files:
      fo.disk = j
    disks.append(j)
  return disks,groups
    

def assign_files_in_order(filenames,numdisks,group):#{{{
  disks = []
  group_dict = {}
  len_files = len(filenames)
  filenames.sort(key = lambda f: int(f.name))

  for f in filenames:
    file_group = f.__dict__[group]
    if file_group not in group_dict:
      group_dict[file_group] = set([f])
    else:
      group_dict[file_group].add(f)
  for fg in group_dict:
    filegroup = Filegroup(group_dict[fg],fg)
    for f in group_dict[fg]:
      f.group = filegroup
    groups.append(filegroup)
    
  max_files_per_disk = int(math.ceil(len_files/float(numdisks)))
  fileindex=0
  for i in range(numdisks):
    if fileindex+max_files_per_disk < len_files:
      files = filenames[fileindex:fileindex+max_files_per_disk]
    else:
      files = filenames[fileindex:]
    fileindex = fileindex+max_files_per_disk
    j = Disk(files)
    for fo in files:
      fo.disk = j
    disks.append(j)
  return disks,groups#}}}

def assign_files_to_random_disks(filenames,numdisks,group,seed):
  '''Assigns files randomly to a specified number of disks'''
  disks = []
  group_dict = {}
  len_files = len(filenames)
  random.seed(seed)
  for f in filenames:
    file_group = f.__dict__[group]
    if file_group not in group_dict:
      group_dict[file_group] = set([f])
    else:
      group_dict[file_group].add(f)
  
  for fg in group_dict:
    filegroup = Filegroup(group_dict[fg],fg)
    for f in group_dict[fg]:
      f.group = filegroup
    groups.append(filegroup)
    
  max_files_per_disk = int(math.ceil(len_files/float(numdisks)))
  random.shuffle(filenames) # randomizes list in place
  fileindex=0
  for i in range(numdisks):
    if fileindex+max_files_per_disk < len_files:
      files = filenames[fileindex:fileindex+max_files_per_disk]
    else:
      files = filenames[fileindex:]
    fileindex = fileindex+max_files_per_disk
    j = Disk(files)
    for fo in files:
      fo.disk = j
    disks.append(j)
  return disks,groups

def assign_files_to_groups_on_disks_evenly(files,group,num_disks):
  '''Assign files to groups based on one of a variety of pre-defined groupings
  and place groups evenly across a set number of disks'''
  file_group_dict={}
  print files[0]
  for file in files:
    file_group = file.__dict__[group]
    if file_group not in file_group_dict:
      file_group_dict[file_group] = set([file])
    else:
      file_group_dict[file_group].add(file)
  # Now files are assigned to file groups.  Let's put the groups onto some disks
  disk_size = math.ceil(float(len(files))/num_disks)
  print "Disk Size is",disk_size
  disks = []
  groups = []
  disk_count = 0
  current_disk = Disk([],str(disk_count))
  checker = 0
  for file_group in file_group_dict:
    fg = Filegroup(file_group_dict[file_group],file_group,[current_disk])
    for file in file_group_dict[file_group]:
      file.group=fg
      if len(current_disk.files)<disk_size:
        current_disk.files.append(file)
        file.disk = current_disk
        checker += 1
      else:
        # Time to start a new disk
        disks.append(current_disk)
        disk_count += 1
        current_disk = Disk([file],str(disk_count))
        fg.disks.append(current_disk)
        file.disk = current_disk
        checker += 1
    groups.append(fg)
  disks.append(current_disk)
  print str(len(disks))+' disks successfully allocated using '+group
  print 'Files',len(files),
  print 'vs ', checker
  return disks,groups

def assign_files_to_groups_disk_per_group(files,group):
  '''Assign files to groups based on one of a variety of pre-defined groupings
  and place each group on a separate Disk object'''
  disks = []
  disk_dict = {}
  for file in files:
    file_group = file.__dict__[group]
    if file_group not in disk_dict:
      a = Disk([file],file_group)
      disks.append(a)
      disk_dict[file_group] = a
    else:
      disk_dict[file_group].files.append(file)
  groups = []
  for fg in disk_dict:
    groups.append(Filegroup(disk_dict[fg].files,fg,disk_dict[fg]))
  print disk_dict.keys()
  return disks,groups

def next_accessible(disk,time,rebuild,failure_prob=.001,spinup_failure_prob=.00001,scrub_factor=.01):
  '''Randomly decides whether a disk is failed when asked and returns the next
  time the disk is accessible'''
  # failure_prob = the probability of a uniform failure at time t
  # spinup_failure_prob = the increase in probability of failure produced by
  # spinning a disk.  This is known to be a primary cause of disk failure.
  # scrub_factor = the reduction in future disk failures caused by scrubbing.
  # Essentially, scrubbing is looking for trouble and pre-loading failure.

  if disk.last_failed + rebuild >= time:
    #print "caught in last failure"
    return disk.last_failed+rebuild
  else:
    # Disk isn't currently failed.  Let's update it's failure probabilities
    spinups = disk.spinups
    
    spin_factor = spinups * spinup_failure_prob
   # For now, lets do it the easy way: exponential + weighting.
    rand = random.uniform(0,1)
    if rand < failure_prob+spin_factor:
      #print "FAILFAILFAIL"
      disk.last_failed = time
      disk.failure_count += 1
      return time+rebuild
    else:
      return time

def step_through_trace(disks,groups,accesses,cache,rebuild,failure_prob=.001,rs=10):
  '''Goes through accesses and simulates a system with failures'''
  '''Some numbers: a 3TB Seagate barracuda disk does sequential reads at about
  150 MB/s (http://www.symantec.com/connect/articles/getting-hang-iops).  Given
  that, it takes about 5.8 hours to read the entire disk.'''
  # Assume for now that accesses are temporally sorted
  l = len(accesses)
  c = 0
  random.seed(rs) # Hardcode a seed to make the fails the same every time.
                  # Random disks are unaffected because they are already
                  # allocated by this point.
  for access in accesses:
    if l % (c+1) == 10000:
      print c,'*',
    c += 1
    f = access.file
    fn = f.name
    fg = f.group
    if fg != "" and fg.last_failure > (access.time-rebuild):
      # Can't proceed until failure is finished
      fg.time_lost += (fg.last_failure+rebuild - access.time)
    if cache.has(fn):
      cache.markhit(fn)
      continue
    else:
      cache.add(fn)
    #disk = get_disk(fn,disks)
    disk = f.disk
    assert f.disk != "",'File %s has no disk!' %'f' 
    disk.spinups +=1
    disk_accessible_time = next_accessible(disk,access.time,rebuild,failure_prob)
    if disk_accessible_time > access.time:
      #print "Tried to access a failed disk.  Waiting to retry"
      disk.time_lost += disk_accessible_time - access.time
      if disk.time_lost == rebuild: # A failure just happened now
        fg.last_failure = access.time
        fg.time_lost += rebuild

def calculate_rebuild_time(disk_size,read_speed,striping):
  # Disk size is in # of files.  Assume files have equal size of 10MB
  total_size = (disk_size*10)/1024.0
  return total_size*read_speed

if __name__ == '__main__':
  ''' Main Simulator
  
type choices:
  random-<group>-<numdisks> : groups randomly split across given disks
  <group>-<numdisks> : groups split across given disks
  <group> : gives you 1 disk / group
  
  to run: 
    python file-based_fault_simulator.py <datafile> <type> <cache> <cache_size>
    [-taciturn]'''

  access_file = sys.argv[1]
  type = sys.argv[2]

  cache_size = 0
  verbose = True
  rebuild_time = datetime.timedelta(seconds=0)
  iterations = 0
  failure_rate = .001
  read_speed = 1 # GB/s
  striping = 5
  seed = 10

  for arg,i in zip(sys.argv[2:],range(len(sys.argv))[2:]):
    if arg == 'cache':
      cache_size = int(sys.argv[i+1])
    elif arg == '-taciturn':
      verbose = False
    elif arg == '-read':
      read_speed = float(sys.argv[i+1])
    elif arg == '-fail':
      failure_rate = float(sys.argv[i+1])
    elif arg == '-rebuild':
      rebuild_time = datetime.timedelta(seconds=int(sys.argv[i+1]))
    elif arg == '-repeat':
      iterations = int(sys.argv[i+1])

  fo = open(access_file,'r')
  type_line = fo.readline()
   
  if type_line.startswith('Record'):#{{{
    accesses, filenames, files = turn_wash_access_file_into_accesses(fo)
  else:
    accesses, filenames, files = turn_water_access_file_into_accesses(fo)

  if len(filenames) != len(files):
    print str(len(filenames))+" filenames"
    print str(len(files))+" files"
    sys.exit()
  print "Accessess indexed"
  print "Unique files identified"#}}}

# This is where the multiprocessing split needs to happen... but it can't
# because the group/disk combination relies on changing the files.

  groups = []#{{{i
  if type in ['site','site_type','district','year','wash']:
    grouping_type = type+'-grouped'
    disks,groups = assign_files_to_groups_disk_per_group(files,type)
  elif type.split('-')[0] in ['site','site_type','district','year','wash']:
    grouping_type = type.split('-')[0]+'-grouped'
    disks,groups = assign_files_to_groups_on_disks_evenly(files,type.split('-')[0],int(type.split('-')[1]))
  elif type.split('-')[0] == 'inorder':
    grouping_type = type.split('-')[1]+'-inorder'
    numdisks = int(type.split('-')[2])
    print "Inorder Run"
    disks, groups = assign_files_in_temp_order(list(files),numdisks,grouping_type.split('-')[0])
  elif type.split('-')[0] == 'random':
    grouping_type = type.split('-')[1]+'-random'
    numdisks = int(type.split('-')[2])
    print "Random Run"
    seed = random.random()
    print seed
    disks,groups = assign_files_to_random_disks(list(files),numdisks,grouping_type.split('-')[0],seed)

  else:
    print "Please specify what sort of disks you want (random, site, site_type,district,year)"#}}}
  
  cache = Cache(cache_size)
  print "Cache Size is "+str(cache_size)
  numdisks = len(disks)
  print str(numdisks)+" disks allocated"
  
  if rebuild_time.seconds == 0:  # Rebuild isn't specified: let's calculate it:
    disk_size = len(list(files))/numdisks
    print 'Disk Size is: '+str(disk_size)
    rebuild_time = datetime.timedelta(seconds=calculate_rebuild_time(disk_size,read_speed,striping))
  print "Rebuild Time is",rebuild_time.seconds 

  if iterations > 0:
    step_through_trace(disks,groups,accesses,cache,rebuild_time)
    count = 0
    fails = 0
    total_time_lost = datetime.timedelta(0)
    total_group_time_lost = datetime.timedelta(0)
    for group in groups:#{{{
      total_group_time_lost += group.time_lost
    for disk in disks:
      count +=1
      fails += disk.failure_count
      total_time_lost += disk.time_lost
    stats = [grouping_type,numdisks,cache_size,rebuild_time.seconds,fails,total_time_lost.total_seconds(),total_group_time_lost.total_seconds(),cache.hits]
    sys.exit()

# The following execution path is only for single runs.

  step_through_trace(disks,groups,accesses,cache,rebuild_time,failure_rate)
  print "Step through trace completed"
  print
  count = 0
  fails = 0
  total_time_lost = datetime.timedelta(0)

  total_group_time_lost = datetime.timedelta(0)
  for group in groups:#{{{
    if verbose:
      print "Group "+str(group.name)+" had "+str(len(group.members))+" members and lost "+str(group.time_lost)
    total_group_time_lost += group.time_lost
    if verbose:
      print
      print
  for disk in disks:
    if verbose:
      print "Disk "+str(count)+" stored "+str(len(disk.files))+" files and Lost "+str(disk.time_lost)
    count +=1
    fails += disk.failure_count
    total_time_lost += disk.time_lost
    if verbose:
      print #}}}
  stats = [grouping_type,numdisks,cache_size,rebuild_time,fails,total_time_lost.total_seconds(),total_group_time_lost.total_seconds(),cache.hits]

  print "There were "+str(fails)+" total failures and "+str(total_time_lost)+' total disk time lost with '+str(total_group_time_lost)+' total group time lost and '+str(cache.hits)+' cache hits.'
  print 
  print 'Grouping type,numdisks, cache size, rebuild time, # failures, total time lost (s), group time lost (s), cache hits'
  print ','.join(map(str,stats))
