import csv
import sys
import datetime
import random

def parse_wash(fileobj):
  # Parses csv washington data and returns an array
  myarray=[]
  myreader = csv.reader(fileobj,delimiter='|',quotechar='"')
  #for row in myreader:
  #  myarray.append(row)
  return myreader

def count_unique_elements(myarray):
  # Returns the number of unique files accessed.
  file_dict = {} # Do this as a dict so we can return max file counts later if
                 # we want to.
  for access in myarray:
    rec_id = access[0]
    if rec_id in file_dict:
      file_dict[rec_id] += 1
    else:
      file_dict[rec_id] = 1
  return len(file_dict.keys()) 

def count_group_elements(myarray):
  # Returns the number of unique members of a group type (e.g. Death Records,
  # Marraige Records)
  # Returns a dictionary of groups and number of elements per group.
  groups = {}
  badness = 0
  for access in myarray:
    if len(access)<5:
      continue
    group = access[0].strip()
    print group
    if group in groups:
      groups[group] += 1
    else:
      groups[group] = 1
  return groups
  #  id = access[0]
  #  if id in ids:
  #    if ids[id]!=group:
  #      badness +=1
  #  ids[id]=group
  #print badness,' groups changed ids'
  #return ids

def print_accesses_with_groups(groups,access_file):
  fo = open(access_file,'r')
  output = open(fo.name+'.groups_added','w')
  for line in fo:
    id = line.strip().split('|')[2]
    if id == '':
      continue
    if id in groups:
      group = groups[id]
    else:
      print line
      group = 'null'
    output.write(group+'|'+line)



if __name__=="__main__":
  fileobj = open(sys.argv[1],'r')
  myarray = parse_wash(fileobj)
  arg = sys.argv[2]
  if arg == 'groups':
    print "The number of unique elements is: "+str(count_unique_elements(myarray))
  elif arg == 'elements':
    print "The group dictionary is: ",count_group_elements(myarray)
  else:
    print_accesses_with_groups(count_group_elements(myarray),arg)
